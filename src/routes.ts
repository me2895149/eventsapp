import express, { Request, Response } from 'express'

const router = express.Router()

router.use(express.json())

interface Event {
    id: number
    title: string
    description: string
    date: string
    time?: string
}

let events: Array<Event> = []

//List of events
router.get('/', (req: Request, res: Response) => {
    res.send(events)   
})

//List of events in given month
router.get('/:monthNumber', (req: Request, res: Response) => {
    const newEvents = events.filter(event => event.date.substring(5, 7) === req.params.monthNumber)
    res.send(newEvents)   
})

//Add new event
router.post('/', (req: Request, res: Response) => {
    const event: Event = req.body
    events.push(event)
    res.status(201).send('New note created adn some testing')
})

//Modify event
router.put('/:eventId', (req: Request, res: Response) => {
    let event = events.find(event => event.id === +(req.params.eventId))
    if (event === undefined) {
        res.status(404).send('Student not found!')
    }
    else {
        if (req.body.title !== undefined) event.title = req.body.title
        if (req.body.description !== undefined) event.description = req.body.description
        if (req.body.date !== undefined) event.date = req.body.date
        if (req.body.time !== undefined) event.time = req.body.time
        }
    res.status(204).send("")
})

//Delete event
router.delete('/:eventId', (req: Request, res: Response) => {
    const newEvents = events.filter(note => note.id !== +(req.params.eventId))

    if (newEvents.length === events.length) {
        return res.status(404).send()
    }
    events = newEvents
    res.status(204).send()
})

export default router
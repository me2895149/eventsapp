FROM node:21-alpine AS builder
WORKDIR /app

COPY ./package*.json ./
COPY ./src ./src
COPY ./tsconfig.json ./tsconfig.json

RUN npm ci
RUN npm run build

FROM node:21-alpine AS final
WORKDIR /app
ENV NOTE_USER=testi
ENV NOTE_PASSWORD=$argon2id$v=19$m=65536,t=3,p=4$x1mw/lfaQ0GWpywNcS4Q6w$rtN7r0N1KqHhLhZqGzaKcln9voQk41OHGyMo5DJlq4Y
COPY --from=builder ./app/dist ./dist
COPY ./package*.json ./
RUN npm ci --production

EXPOSE 3000
CMD ["npm", "start"]